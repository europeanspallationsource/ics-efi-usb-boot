#platform=x86, AMD64, or Intel EM64T
# System authorization information
auth  --useshadow  --enablemd5
# Use text mode install
text
# Firewall configuration
firewall --enabled --service=ssh
# Run the Setup Agent on first boot
firstboot --disable
# System keyboard
keyboard sv-latin1
# System language
lang en_US
# Use network installation
repo --name=base --baseurl=http://install01.esss.lu.se/centos7
reboot
#Root password (arcos84Clubb)
rootpw --iscrypted $1$qBSnp4Ld$OGERYahErZKhZFkQXnb0w0

# SELinux configuration
selinux --disabled
# Do not configure the X Window System
skipx
# System timezone
timezone  Europe/Stockholm --isUtc

# Clear the Master Boot Record
zerombr
# System bootloader configuration
bootloader --location=mbr
# Partition clearing information
clearpart --all --initlabel
# Allow anaconda to partition the system as needed
autopart

%pre
# Enable installation monitoring
%end

%packages
kernel
@core
yum-plugin-fastestmirror
grub2
grub2-efi
efibootmgr
shim
%end

%post --nochroot
touch $INSTALL_ROOT/this-is-install-root #(/)
touch $LIVE_ROOT/this-is-live-root #(/run/initramfs/live/)
rm -rf $LIVE_ROOT/isolinux/isolinux.cfg
cat >> $LIVE_ROOT/isolinux/isolinux.cfg << EOF_isolinux

default vesamenu.c32
timeout 100
menu background 
menu autoboot Starting ESSInstall in # second{,s}. Press any key to interrupt.

menu clear
menu title ESSInstall
menu vshift 8
menu rows 18
menu margin 8
#menu hidden
menu helpmsgrow 15
menu tabmsgrow 13

menu color border * #00000000 #00000000 none
menu color sel 0 #ffffffff #00000000 none
menu color title 0 #ff7ba3d0 #00000000 none
menu color tabmsg 0 #ff3a6496 #00000000 none
menu color unsel 0 #84b8ffff #00000000 none
menu color hotsel 0 #84b8ffff #00000000 none
menu color hotkey 0 #ffffffff #00000000 none
menu color help 0 #ffffffff #00000000 none
menu color scrollbar 0 #ffffffff #ff355594 none
menu color timeout 0 #ffffffff #00000000 none
menu color timeout_msg 0 #ffffffff #00000000 none
menu color cmdmark 0 #84b8ffff #00000000 none
menu color cmdline 0 #ffffffff #00000000 none

menu tabmsg Press Tab for full configuration options on menu items.
menu separator
menu separator
label linux0
  menu label ^Start ESS
  kernel vmlinuz0
  append initrd=initrd0.img root=live:CDLABEL=live rootfstype=auto ro rd.live.image quiet  rhgb rd.luks=0 rd.md=0 rd.dm=0 
  menu default
menu separator
menu begin ^Troubleshooting
  menu title Troubleshooting
label basic0
  menu label Start Live in ^basic graphics mode.
  kernel vmlinuz0
  append initrd=initrd0.img root=live:CDLABEL=live rootfstype=auto ro rd.live.image quiet  rhgb rd.luks=0 rd.md=0 rd.dm=0 nomodeset
  text help
      Try this option out if you're having trouble starting.
  endtext
menu separator
label local
  menu label Boot from ^local drive
  localboot 0xffff
menu separator
label returntomain
  menu label Return to ^main menu.
  menu exit
menu end
EOF_isolinux

mv $LIVE_ROOT/EFI/BOOT/grub.cfg $LIVE_ROOT/EFI/BOOT/grub.old
#cat >> $LIVE_ROOT/EFI/BOOT/grub.cfg << EOF_efilinux
cat >> $INSTALL_ROOT/boot/efi/EFI/centos/grub.cfg

set default="1"

function load_video {
  insmod efi_gop
  insmod efi_uga
  insmod video_bochs
  insmod video_cirrus
  insmod all_video
}

load_video
set gfxpayload=keep
insmod gzio
insmod part_gpt
insmod ext2

set timeout=10
### END /etc/grub.d/00_header ###

search --no-floppy --set=root -l 'live'

### BEGIN /etc/grub.d/10_linux ###
menuentry 'Start ESS' --class fedora --class gnu-linux --class gnu --class os {
	linuxefi /isolinux/vmlinuz0 root=live:LABEL=live ro rd.live.image quiet  rhgb 
	initrdefi /isolinux/initrd0.img
}

submenu 'Troubleshooting -->' {
menuentry 'Start Live in basic graphics mode' --class fedora --class gnu-linux --class gnu --class os {
	linuxefi /isolinux/vmlinuz0 root=live:LABEL=live ro rd.live.image quiet  rhgb nomodeset
	initrdefi /isolinux/initrd0.img
}
}

EOF_efilinux

%end

%post
#me
%end
